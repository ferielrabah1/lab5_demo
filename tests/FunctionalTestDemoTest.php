<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTestDemoTest extends WebTestCase
{
    public function testShouldAddNewDemo()
    {
        $client = static::createClient();
        $client->followRedirects();

        // Access the form to create a new Demo
        $crawler = $client->request('GET', '/demo/new');
        $this->assertResponseIsSuccessful();

        // Fill out the form and submit
        $uuid = uniqid();
        $form = $crawler->filter('form[name=demo]')->form([
            'demo[demo]' => 'Add Demo For Test ' . $uuid,
        ]);

        $client->submit($form);

        // Check if the form submission was successful
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Add Demo For Test ' . $uuid);

        // Additional assertions or checks if needed
    }
}
